package com.springapp.mvc;

import java.util.Calendar;
import java.util.TimeZone;

public class FormalGreeter implements IGreeter {
    private String tz = null;

    public String getTz() {
        return tz;
    }

    public void setTz(String tz) {
        this.tz = tz;
    }

    @Override
    public String greet() {
        final TimeZone timeZone = (getTz() != null) ?
                TimeZone.getTimeZone(getTz()) :
                TimeZone.getDefault();
        final int hour = Calendar.getInstance(timeZone).get(Calendar.HOUR_OF_DAY);
        if (hour >= 21 && hour < 6) {
            return "Good night";
        }
        else if (hour >= 6 && hour < 12) {
            return "Good morning";
        }
        else if (hour >= 12 && hour < 18) {
            return "Good afternoon";
        }
        else {
            return "Good evening";
        }
    }
}
